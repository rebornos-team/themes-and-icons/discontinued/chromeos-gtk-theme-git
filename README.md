# chromeos-gtk-theme-git

A Material Design theme for GNOME/GTK based desktop environments.

https://github.com/vinceliuice/chromeos-theme

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/themes-and-icons/chromeos-gtk-theme-git.git
```

